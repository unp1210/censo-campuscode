# frozen_string_literal: true

require 'spec_helper'

describe 'StatesController' do
  it '#all' do
    state = StatesController
    expect(state).to receive(:all)
    state.all
  end
end
