# frozen_string_literal: true

require 'spec_helper'

describe 'NameByCityController' do
  it '#get_all_name' do
    name = NameByCityController
    expect(name).to receive(:get_all_name)
    name.get_all_name('5300108')
  end

  it '#by_gender' do
    name_controller = NameByCityController
    expect(name_controller).to receive(:by_gender)
    name_controller.by_gender('5300108', 'm')
  end
end
