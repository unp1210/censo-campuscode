# frozen_string_literal: true

require 'spec_helper'

describe 'NameByStateController' do
  it '#get_all_name' do
    name_controller = NameByStateController
    expect(name_controller).to receive(:get_all_name)
    name_controller.get_all_name('35')
  end

  it '#by_gender' do
    name_controller = NameByStateController
    expect(name_controller).to receive(:by_gender)
    name_controller.by_gender('35', 'm')
  end
end
