# frozen_string_literal: true

require 'spec_helper'

describe 'NameByFrequencyController' do
  it '#all' do
    name = NameFrequencyController
    expect(name).to receive(:list_name)
    name.list_name('JOAO')
  end
end
