# frozen_string_literal: true

require 'spec_helper'

describe 'CityController' do
  it '#all' do
    city = CityController
    expect(city).to receive(:all)
    city.all('45')
  end
end
