# frozen_string_literal: true

require 'spec_helper'

describe 'RankingNameState' do
  it '#list_name' do
    json_content = File.read('spec/support/apis/state/get_name_by_state.json')
    faraday_response = double('states', status: 200, body: json_content)
    allow(Faraday).to receive(:get).and_return(faraday_response)

    names = NamesByState.by_state('33')
    expect(RankingNameState).to receive(:list_all_state).with(names)
    expect { tp.set :io, $stdout; tp names }.to output(a_string_including('MARIA', 'JOSE')).to_stdout
    RankingNameState.list_all_state(names)
  end

  it '#list_gender' do
    json_content = File.read('spec/support/apis/state/get_name_by_state_gender.json')
    faraday_response = double('states', status: 200, body: json_content)
    allow(Faraday).to receive(:get).and_return(faraday_response)

    names = NamesByState.by_gender('33', 'feminino')
    expect(RankingNameState).to receive(:list_gender).with(names)
    expect { tp.set :io, $stdout; tp names }.to output(a_string_including('MARIA', 'ANA')).to_stdout
    RankingNameState.list_gender(names)
  end
end
