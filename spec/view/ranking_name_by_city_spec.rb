# frozen_string_literal: true

require 'spec_helper'

describe 'RankingNameCity' do
  it '#list_name' do
    json_content = File.read('spec/support/apis/city/get_name_by_city.json')
    faraday_response = double('states', status: 200, body: json_content)
    allow(Faraday).to receive(:get).and_return(faraday_response)

    names = NamesByCity.by_city(3_504_107)
    expect(RankingNameCity).to receive(:list_name).with(names)
    expect { tp.set :io, $stdout; tp names }.to output(a_string_including('MARIA', 'JOSE')).to_stdout
    RankingNameCity.list_name(names)
  end

  it '#list_gender' do
    json_content = File.read('spec/support/apis/city/get_name_by_city_gender.json')
    faraday_response = double('states', status: 200, body: json_content)
    allow(Faraday).to receive(:get).and_return(faraday_response)

    names = NamesByCity.by_gender(3_504_107, 'feminino')
    expect(RankingNameCity).to receive(:list_gender).with(names)
    expect { tp.set :io, $stdout; tp names }.to output(a_string_including('JOSE', 'JOAO')).to_stdout
    RankingNameCity.list_gender(names)
  end
end
