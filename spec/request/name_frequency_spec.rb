# frozen_string_literal: true

require 'spec_helper'

describe 'check api name frequency' do
  context 'get name' do
    it 'sucessfully one name' do
      json_content = File.read('spec/support/apis/period/get_name_frequency.json')
      faraday_response = double('frequency', status: 200, body: json_content)
      allow(Faraday).to receive(:get).and_return(faraday_response)

      result = NameFrequency.every_decade('JOAO|MARIA')

      expect(result.length).to eq 2
      expect(result[0].name).to eq 'JOAO'
      expect(result[0].locale).to eq 'BR'
      expect(result[0].period[0]).to eq '1930'
      expect(result[0].frequency[0]).to eq 60_155
      expect(result[1].name).to eq 'MARIA'
      expect(result[1].locale).to eq 'BR'
      expect(result[1].period[0]).to eq '1930'
      expect(result[1].frequency[0]).to eq 336_477
    end

    it 'error API' do
      faraday_response = double('frequency', status: 500)
      allow(Faraday).to receive(:get).and_return(faraday_response)

      result = NameFrequency.every_decade('')

      expect(result.length).to eq 0
    end
  end
end
