# frozen_string_literal: true

require 'spec_helper'

describe 'check api states' do
  context 'get all states' do
    it 'fetch all states from api' do
      json_content = File.read('spec/support/apis/state/get_states.json')
      faraday_response = double('states', status: 200, body: json_content)
      allow(Faraday).to receive(:get).and_return(faraday_response)

      result = State.all

      expect(result.length).to eq 2
      expect(result[0].name).to eq 'Rondonia'
      expect(result[0].initial).to eq 'RO'
      expect(result[1].name).to eq 'Amazonia'
      expect(result[1].initial).to eq 'AM'
    end
  end
end
