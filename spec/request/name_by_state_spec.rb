# frozen_string_literal: true

require 'spec_helper'

describe 'check api name by state' do
  context 'get all name' do
    it 'fetch all gender from api' do
      json_content = File.read('spec/support/apis/state/get_name_by_state.json')
      faraday_response = double('states', status: 200, body: json_content)
      allow(Faraday).to receive(:get).and_return(faraday_response)

      result = NamesByState.by_state(33)

      expect(result.length).to eq 2
      expect(result[0].nome).to eq 'MARIA'
      expect(result[0].ranking).to eq 1
    end

    it 'fetch by gender' do
      json_content = File.read('spec/support/apis/state/get_name_by_state_gender.json')
      faraday_response = double('states', status: 200, body: json_content)
      allow(Faraday).to receive(:get).and_return(faraday_response)

      result = NamesByState.by_gender(33, 'feminino')

      expect(result.length).to eq 2
      expect(result[0].nome).to eq 'MARIA'
      expect(result[0].sexo).to eq 'f'
      expect(result[0].ranking).to eq 1
      expect(result[1].nome).to eq 'ANA'
      expect(result[1].sexo).to eq 'f'
      expect(result[1].ranking).to eq 2
    end

    it 'error API' do
      faraday_response = double('states', status: 500)
      allow(Faraday).to receive(:get).and_return(faraday_response)

      result = NamesByState.by_state(0o00)

      expect(result.length).to eq 0
    end
  end
end
