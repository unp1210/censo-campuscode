# frozen_string_literal: true

require 'spec_helper'

describe 'check api name by city' do
  it 'fetch all gender from api' do
    json_content = File.read('spec/support/apis/city/get_name_by_city.json')
    faraday_response = double('cities', status: 200, body: json_content)
    allow(Faraday).to receive(:get).and_return(faraday_response)

    result = NamesByCity.by_city(3_504_107)
    expect(result.length).to eq 2
    expect(result[0].nome).to eq 'MARIA'
    expect(result[0].ranking).to eq '1'
  end

  it 'fetch by gender' do
    json_content = File.read('spec/support/apis/city/get_name_by_city_gender.json')
    faraday_response = double('cities', status: 200, body: json_content)
    allow(Faraday).to receive(:get).and_return(faraday_response)

    result = NamesByCity.by_gender(3_504_107, 'masculino')

    expect(result.length).to eq 2
    expect(result[0].nome).to eq 'JOSE'
    expect(result[0].sexo).to eq 'm'
    expect(result[0].ranking).to eq 1
    expect(result[1].nome).to eq 'JOAO'
    expect(result[1].sexo).to eq 'm'
    expect(result[1].ranking).to eq 2
  end

  it 'error API' do
    faraday_response = double('cities', status: 500)
    allow(Faraday).to receive(:get).and_return(faraday_response)

    result = NamesByCity.by_city(0o00)

    expect(result.length).to eq 0
  end
end
