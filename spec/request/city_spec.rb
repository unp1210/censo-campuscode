# frozen_string_literal: true

require 'spec_helper'

describe 'check api cities' do
  context 'get all cities' do
    it 'fetch all cities from api' do
      json_content = File.read('spec/support/apis/city/get_city_by_state.json')
      faraday_response = double('cities', status: 200, body: json_content)
      allow(Faraday).to receive(:get).and_return(faraday_response)

      result = City.all(35)

      expect(result.length).to eq 2
      expect(result[0].name).to eq 'Atibaia'
      expect(result[1].name).to eq 'Adolfo'
    end

    it 'error API' do
      faraday_response = double('states', status: 500)
      allow(Faraday).to receive(:get).and_return(faraday_response)

      result = City.all(0o0)

      expect(result.length).to eq 0
    end
  end
end
