# frozen_string_literal: true

require 'spec_helper'

describe 'store data state' do
  it 'select successfully' do
    DatabaseState.insert(11, 'RO', 'Rondonia')

    states = DatabaseState.all

    expect(states.count).to eq 1
    expect(states[0].name).to eq 'Rondonia'
  end

  it 'insert successfully' do
    expect(DatabaseState.insert(13, 'AM', 'Amazonia')).to eq(true)
  end

  it 'and pkey alredy exist' do
    error = DatabaseState.insert(11, 'RO', 'Rondonia')

    expect(error.message).to include('duplicate key value violates unique constraint')
  end
end
