# frozen_string_literal: true

require 'spec_helper'

describe 'store data state' do
  it 'select successfully' do
    DatabaseNameByCity.insert(35_041_071, 'MARIA', 1, nil)

    names = DatabaseNameByCity.all(35_041_071)

    expect(names.count).to eq 1
    expect(names[0].nome).to eq 'MARIA'
  end

  it 'insert successfully' do
    expect(DatabaseNameByCity.insert(35_041_072, 'JOSE', 2, nil)).to eq(true)
  end

  it 'and pkey alredy exist' do
    error = DatabaseNameByCity.insert(35_041_072, 'JOSE', 1, nil)

    expect(error.message).to include('duplicate key value violates unique constraint')
  end
end
