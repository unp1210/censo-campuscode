# frozen_string_literal: true

require 'spec_helper'

describe 'store data state' do
  it 'select successfully' do
    DatabaseGenderByCity.insert('35041071m', 'JOSE', 1, 'm')

    names = DatabaseGenderByCity.all('35041071m', 'm')

    expect(names.count).to eq 1
    expect(names[0].nome).to eq 'JOSE'
    expect(names[0].sexo).to eq 'm'
  end

  it 'insert successfully' do
    expect(DatabaseGenderByCity.insert('35041072m', 'JOAO', 2, 'm')).to eq(true)
  end

  it 'and pkey alredy exist' do
    error = DatabaseGenderByCity.insert('35041072m', 'JOSE', 1, 'm')

    expect(error.message).to include('duplicate key value violates unique constraint')
  end
end
