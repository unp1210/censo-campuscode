# frozen_string_literal: true

require 'spec_helper'

describe 'store data city' do
  it 'select successfully' do
    DatabaseCity.insert(3_504_107, 'Atibaia')

    cities = DatabaseCity.all(3_504_107)

    expect(cities.count).to eq 1
    expect(cities[0].name).to eq 'Atibaia'
  end

  it 'insert successfully' do
    expect(DatabaseCity.insert(3_500_204, 'Adolfo')).to eq(true)
  end

  it 'and pkey alredy exist' do
    error = DatabaseCity.insert(3_500_204, 'Adolfo')

    expect(error.message).to include('duplicate key value violates unique constraint')
  end
end
