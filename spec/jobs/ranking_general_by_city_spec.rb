# frozen_string_literal: true

require 'spec_helper'

describe 'store general ranking by city' do
  it 'successfully' do
    json_content = File.read('spec/support/apis/city/get_name_by_city.json')
    faraday_response = double('cities', status: 200, body: json_content)
    allow(Faraday).to receive(:get).and_return(faraday_response)

    StoreRankingCity.new.perform(35)
    result = DatabaseNameByCity.all(35)

    expect(result.count).to eq 2
    expect(result[0].nome).to eq 'MARIA'
    expect(result[1].nome).to eq 'JOSE'
  end
end
