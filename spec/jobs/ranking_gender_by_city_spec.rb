# frozen_string_literal: true

require 'spec_helper'

describe 'store general ranking by city' do
  it 'successfully' do
    json_content = File.read('spec/support/apis/city/get_name_by_city_gender.json')
    faraday_response = double('cities', status: 200, body: json_content)
    allow(Faraday).to receive(:get).and_return(faraday_response)

    StoreGenderCity.new.perform(35, 'm')
    result = DatabaseGenderByCity.all(35, 'm')

    expect(result.count).to eq 2
    expect(result[0].nome).to eq 'JOSE'
    expect(result[0].ranking).to eq '1'
    expect(result[1].nome).to eq 'JOAO'
    expect(result[1].ranking).to eq '2'
  end
end
