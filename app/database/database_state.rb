# frozen_string_literal: true

require 'pg'
# This class store all states
class DatabaseState
  attr_reader :id, :initial, :name

  def initialize(id:, initial:, name:)
    @id = id
    @initial = initial
    @name = name
  end

  def self.insert(id, initial, name)
    con = PG::Connection.new(host: 'postgres', user: 'postgres', password: 'postgres')
    con.exec "INSERT INTO states VALUES(#{id},'#{initial}','#{name}')"
    con.close
    true
  rescue StandardError => e
    e
  end

  def self.all
    con = PG::Connection.new(host: 'postgres', user: 'postgres', password: 'postgres')
    states = con.exec 'SELECT * FROM states'
    con.close
    states.map do |state|
      new(id: state['id'], initial: state['initial'], name: state['name'])
    end
  end
end
