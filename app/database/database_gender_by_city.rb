# frozen_string_literal: true

# This class store ranking names/gender by city
class DatabaseGenderByCity
  attr_reader :id, :nome, :ranking, :sexo

  def initialize(id:, nome:, ranking:, sexo:)
    @id = id
    @nome = nome
    @ranking = ranking
    @sexo = sexo
  end

  def self.insert(id, nome, ranking, sexo)
    con = PG::Connection.new(host: 'postgres', user: 'postgres', password: 'postgres')
    con.exec "INSERT INTO gender_by_city VALUES('#{id}', '#{nome}', '#{ranking}', '#{sexo}')"
    con.close
    true
  rescue StandardError => e
    e
  end

  def self.all(id, gender)
    con = PG::Connection.new(host: 'postgres', user: 'postgres', password: 'postgres')
    names = con.exec "SELECT * FROM gender_by_city WHERE gender_by_city.id LIKE '#{id}%'
                      AND gender_by_city.sexo LIKE '#{gender}'"
    con.close
    names.map do |name|
      new(id: name['id'], nome: name['name'], ranking: name['ranking'], sexo: name['sexo'])
    end
  end
end
