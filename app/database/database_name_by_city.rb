# frozen_string_literal: true

# This class store names by city
class DatabaseNameByCity
  attr_reader :id, :nome, :ranking, :sexo

  def initialize(id:, nome:, ranking:, sexo:)
    @id = id
    @nome = nome
    @ranking = ranking
    @sexo = sexo
  end

  def self.insert(id, nome, ranking, sexo)
    con = PG::Connection.new(host: 'postgres', user: 'postgres', password: 'postgres')
    con.exec "INSERT INTO name_by_city VALUES(#{id}, '#{nome}', '#{ranking}', '#{sexo}')"
    con.close
    true
  rescue StandardError => e
    e
  end

  def self.all(id)
    con = PG::Connection.new(host: 'postgres', user: 'postgres', password: 'postgres')
    names = con.exec "SELECT * FROM name_by_city WHERE name_by_city.id LIKE '#{id}%'"
    con.close
    names.map do |name|
      new(id: name['id'], nome: name['name'], ranking: name['ranking'], sexo: name['sexo'])
    end
  end
end
