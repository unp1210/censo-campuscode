# frozen_string_literal: true

# This class store all city
class DatabaseCity
  attr_reader :id, :name

  def initialize(id:, name:)
    @id = id
    @name = name
  end

  def self.insert(id, name)
    con = PG::Connection.new(host: 'postgres', user: 'postgres', password: 'postgres')
    con.exec "INSERT INTO city VALUES(#{id}, '#{name.gsub(/'/, ' ')}')"
    con.close
    true
  rescue StandardError => e
    e
  end

  def self.all(id)
    con = PG::Connection.new(host: 'postgres', user: 'postgres', password: 'postgres')
    cities = con.exec "SELECT * FROM city WHERE city.id LIKE '#{id}%'"
    con.close
    cities.map do |city|
      new(id: city['id'], name: city['name'])
    end
  end
end
