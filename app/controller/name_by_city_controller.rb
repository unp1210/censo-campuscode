# frozen_string_literal: true

#  Class responsible for calling class NamesByCity and View
class NameByCityController
  def self.get_all_name(city_id)
    names = NamesByCity.by_city(city_id)
    RankingNameCity.list_name(names)
  end

  def self.by_gender(city_id, gender)
    names = NamesByCity.by_gender(city_id, gender)
    RankingNameCity.list_gender(names)
  end
end
