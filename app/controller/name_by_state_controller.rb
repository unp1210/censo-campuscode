# frozen_string_literal: true

# Class responsible for calling class NamesByState and View
class NameByStateController
  def self.get_all_name(locale)
    names = NamesByState.by_state(locale)
    RankingNameState.list_name(names)
  end

  def self.by_gender(locale, gender)
    name_by_gender = NamesByState.by_gender(locale, gender)
    RankingNameState.list_gender(name_by_gender)
  end
end
