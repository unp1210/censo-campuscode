# frozen_string_literal: true

# Class responsible for calling class City and View
class CityController
  def self.all(locale)
    cities = City.all(locale)
    RankingNameCity.all_city(cities)
  end
end
