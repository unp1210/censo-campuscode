# frozen_string_literal: true

# Class responsible for calling class NameFrequency and View
class NameFrequencyController
  def self.list_name(name)
    names = NameFrequency.every_decade(name)
    RankingFrequencyName.list_frequency(names)
  end
end
