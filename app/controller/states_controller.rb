# frozen_string_literal: true

# Class responsible for calling class State and View
class StatesController
  def self.all
    states = State.all
    RankingNameState.list_all_state(states)
  end
end
