# frozen_string_literal: true

require 'sidekiq'
require_relative '../../config/sidekiq'

class StoreRankingCity
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(state_id)
    names = DatabaseNameByCity.all(state_id)
    return unless names.empty?

    City.all(state_id).each do |city|
      NamesByCity.by_city(city.id).map do |name|
        save_database(name)
      end
    end
  end

  private

  def save_database(name)
    DatabaseNameByCity.insert("#{name.localidade}#{name.ranking}", name.nome, name.ranking, name.sexo)
  end
end
