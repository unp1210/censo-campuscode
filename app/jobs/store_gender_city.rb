# frozen_string_literal: true

require 'sidekiq'
require_relative '../../config/sidekiq'

class StoreGenderCity
  include Sidekiq::Worker
  sidekiq_options retry: 3

  def perform(state_id, gender)
    names = DatabaseGenderByCity.all(state_id, gender)
    return unless names.empty?

    City.all(state_id).each do |city|
      NamesByCity.by_gender(city.id, gender).map do |name|
        save_database(name)
      end
    end
  end

  private

  def save_database(name)
    DatabaseGenderByCity.insert("#{name.localidade}#{name.ranking}#{name.sexo}", name.nome, name.ranking, name.sexo)
  end
end
