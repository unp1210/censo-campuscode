# frozen_string_literal: true

# This class shows all cities by state
class RankingNameCity < BaseView
  def self.all_city(cities)
    tp cities
  end
end
