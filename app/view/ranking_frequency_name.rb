# frozen_string_literal: true

# This class show names by period
class RankingFrequencyName
  def self.list_frequency(names)
    return puts 'NENHUM NOME FOI ENCONTRADO' if names.empty?

    names.each do |person|
      puts "PERIODO -      #{person.name}"
      puts '*********************'
      (0..(person.period.size - 1)).each do |j|
        puts "#{person.period[j].rjust(9)} ->    #{person.frequency[j]}"
      end
      puts '*********************'
    end
  end
end
