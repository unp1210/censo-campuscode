# frozen_string_literal: true

# This class is the home view
class Home
  def run
    option = 'true'
    while option.downcase != 'sair'
      puts '1. Ranking dos nomes mais comuns em uma determinada Unidade Federativa (UF)'
      puts '2. Ranking dos nomes mais comuns em uma determinada cidade'
      puts '3. Frequência do uso de um nome ao longo dos anos'
      puts '4. Armazenar ranking de todas as cidades de uma UF'
      puts 'Digite uma das opções acima ou SAIR para finalizar a aplicação'

      option = gets.chomp

      case option.to_i
      when 1
        StatesController.all
        puts 'Selecione o codigo do Estado que deseja consultar'
        locale = gets.chomp
        system('clear')
        NameByStateController.get_all_name(locale)
        NameByStateController.by_gender(locale, 'f')
        NameByStateController.by_gender(locale, 'm')

      when 2
        StatesController.all
        puts 'Selecione o codigo do Estado que deseja consultar'
        locale = gets.chomp
        system('clear')
        CityController.all(locale)
        puts 'Selecione o codigo da cidade que deseja consultar'
        city_id = gets.chomp
        system('clear')
        NameByCityController.get_all_name(city_id)
        NameByCityController.by_gender(city_id, 'f')
        NameByCityController.by_gender(city_id, 'm')

      when 3
        puts 'Digite um ou dois nomes separados apenas por virgula'
        name = gets.chomp
        system('clear')
        NameFrequencyController.list_name(name)

      when 4
        StatesController.all
        puts 'Selecione o codigo do Estado que deseja armazenar'
        state_id = gets.chomp
        system('clear')
        StoreRankingCity.perform_async(state_id)
        StoreGenderCity.perform_async(state_id, 'f')
        StoreGenderCity.perform_async(state_id, 'm')

      else
        system('clear')
        if option.downcase == 'sair'
          puts '*****************'
          puts 'SESSÃO FINALIZADA'
          puts '*****************'
        else
          puts '*******************************'
          puts 'OPÇÃO INVALIDA, TENTE NOVAMENTE'
          puts '*******************************'
        end
      end

    end
  end
end
