# frozen_string_literal: true

# this class will be inherited by other views
class BaseView
  def self.list_name(names)
    tp names, :ranking, :nome
  end

  def self.list_gender(names)
    tp names, :ranking, :nome, :sexo
  end
end
