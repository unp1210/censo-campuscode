# frozen_string_literal: true

# This class shows all states
class RankingNameState < BaseView
  def self.list_all_state(states)
    tp states
  end
end
