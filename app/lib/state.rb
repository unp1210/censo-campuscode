# frozen_string_literal: true

require 'yaml'
# This class check api states
class State
  attr_reader :id, :sigla, :nome

  def initialize(id:, sigla:, nome:)
    @id = id
    @sigla = sigla
    @nome = nome
  end

  def self.all
    states = DatabaseState.all
    return states unless states.empty?

    return [] unless response.status == 200

    list = JSON.parse(response.body, symbolize_names: true)
    list.map do |item|
      DatabaseState.insert(item[:id], item[:sigla], item[:nome])
      states = new(id: item[:id], sigla: item[:sigla], nome: item[:nome])
    end
  end

  def self.response
    Faraday.get (path['development']['states']).to_s
  end

  def self.path
    YAML.safe_load(File.read('config/api.yml'))
  end
end
