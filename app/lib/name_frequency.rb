# frozen_string_literal: true

class NameFrequency
  attr_reader :name, :locale, :period, :frequency

  def initialize(name:, locale:, period:, frequency:)
    @name = name
    @locale = locale
    @period = period
    @frequency = frequency
  end

  def self.every_decade(name)
    names = name.gsub(',', '%7C')
    return [] unless response(names).status == 200

    list = JSON.parse(response(names).body, symbolize_names: true)
    list.map do |person|
      period = []
      frequency = []
      person[:res].map do |item|
        period << item[:periodo].scan(/\d+/).join('-')
        frequency << item[:frequencia]
      end
      new(locale: person[:localidade], name: person[:nome],
          frequency: frequency, period: period)
    end
  end

  def self.response(name)
    Faraday.get "#{path['development']['frequency']}/#{name}"
  end

  def self.path
    YAML.safe_load(File.read('config/api.yml'))
  end
end
