# frozen_string_literal: true

# This class call API name/gender by city
class NamesByCity
  attr_reader :localidade, :nome, :ranking, :sexo

  def initialize(localidade:, nome:, ranking:, sexo:)
    @localidade = localidade
    @nome = nome
    @ranking = ranking
    @sexo = sexo
  end

  def self.by_city(city_id)
    names = DatabaseNameByCity.all(city_id)
    return names unless names.empty?

    return [] unless response(city_id, 'nil').status == 200 && response(city_id, 'nil').body != '[]'

    list = JSON.parse(response(city_id, 'nil').body, symbolize_names: true)
    list[0][:res].map do |item|
      new(localidade: list[0][:localidade], nome: item[:nome],
          ranking: item[:ranking], sexo: item[:sexo])
    end
  end

  def self.by_gender(city_id, gender)
    names = DatabaseGenderByCity.all(city_id, gender)
    return names unless names.empty?

    return [] unless response(city_id, gender).status == 200 && response(city_id, gender).body != '[]'

    list = JSON.parse(response(city_id, gender).body, symbolize_names: true)
    list[0][:res].map do |item|
      new(localidade: list[0][:localidade], sexo: list[0][:sexo], nome: item[:nome], ranking: item[:ranking])
    end
  end

  def self.response(locale, gender)
    Faraday.get "#{path['development']['name_by_city']}#{locale}&sexo=#{gender}"
  end

  def self.path
    YAML.safe_load(File.read('config/api.yml'))
  end
end
