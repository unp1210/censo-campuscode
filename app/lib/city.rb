# frozen_string_literal: true

require_relative '../jobs/store_ranking_city'
# This class api city
class City
  attr_reader :id, :city

  def initialize(id:, city:)
    @id = id
    @city = city
  end

  def self.all(state_id)
    cities = DatabaseCity.all(state_id)
    return cities unless cities.empty?

    return [] unless response(state_id).status == 200

    list = JSON.parse(response(state_id).body, symbolize_names: true)
    list.map do |item|
      DatabaseCity.insert(item[:id], item[:nome])
      new(id: item[:id], city: item[:nome])
    end
  end

  def self.response(state_id)
    Faraday.get "#{path['development']['states']}/#{state_id}/municipios"
  end

  def self.path
    YAML.safe_load(File.read('config/api.yml'))
  end
end
