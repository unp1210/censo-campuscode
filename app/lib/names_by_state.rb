# frozen_string_literal: true

# This class call API name/gender by state
class NamesByState
  attr_reader :localidade, :nome, :ranking, :sexo

  def initialize(localidade:, nome:, ranking:, sexo:)
    @localidade = localidade
    @nome = nome
    @ranking = ranking
    @sexo = sexo
  end

  def self.by_state(locale)
    return [] unless response(locale, 'nil').status == 200

    list = JSON.parse(response(locale, 'nil').body, symbolize_names: true)
    list[0][:res].map do |item|
      new(localidade: list[0][:localidade], nome: item[:nome],
          ranking: item[:ranking], sexo: item[:sexo])
    end
  end

  def self.by_gender(locale, gender)
    return [] unless response(locale, gender).status == 200

    list = JSON.parse(response(locale, gender).body, symbolize_names: true)
    list[0][:res].map do |item|
      new(localidade: list[0][:localidade], sexo: list[0][:sexo], nome: item[:nome],
          ranking: item[:ranking])
    end
  end

  def self.response(locale, gender)
    Faraday.get "#{path['development']['name_by_state']}#{locale}&sexo=#{gender}"
  end

  def self.path
    YAML.safe_load(File.read('config/api.yml'))
  end
end
