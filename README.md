# Ranking iterativo de nomes mais populares do CENSO|IBGE
# Interative ranking of most popular names based CENSO|IBGE data

# Ruby version
* 2.7.2

# What is necessary?
* [Docker](https://docs.docker.com/engine/install/ubuntu/)
* [Docker-compose](https://docs.docker.com/compose/install/)

# Gems
* activesupport
* byebug
* faraday
* json
* pg
* respec
* rubocop
* rspec
* sidekiq
* simplecov
* table_print

# Getting Start
* step 1: type "docker-compose build" => build image
* step 2: type "docker-compose run app bash" => start all the services and configuration
* step 3: type "bin/setup" => install all dependencies
* step 4: type "ruby app.rb" => start application

# Working with sidekiq
* Open a new terminal and follow bellow steps
* step 1: Follow the previous steps until step 3
* step 2: type "./sidekiq" or optionally "bundler exec sidekiq -r ./config/sidekiq.rb"
* step 3: type "ruby app.rb"

# How to run the test suite
* type "rspec" to run tests