#!/usr/bin/ruby
# frozen_string_literal: true

require 'pg'

begin
  con = PG::Connection.new(host: 'postgres', user: 'postgres', password: 'postgres')
  con.exec 'DROP TABLE IF EXISTS states'
  con.exec 'DROP TABLE IF EXISTS city'
  con.exec 'DROP TABLE IF EXISTS name_by_state'
  con.exec 'DROP TABLE IF EXISTS name_by_city'
  con.exec 'DROP TABLE IF EXISTS gender_by_city'
  con.exec "CREATE TABLE states(id INTEGER PRIMARY KEY,
              initial CHAR(2), name VARCHAR(20))"
  con.exec "CREATE TABLE city(id CHAR(7) PRIMARY KEY,
              name VARCHAR(50))"
  con.exec "CREATE TABLE name_by_city(id CHAR(9) PRIMARY KEY,
            name VARCHAR(50), ranking INTEGER, sexo CHAR(1))"
  con.exec "CREATE TABLE gender_by_city(id CHAR(10) PRIMARY KEY,
            name VARCHAR(50), ranking INTEGER, sexo CHAR(1))"
  con.exec "CREATE TABLE name_by_state(id CHAR(2) PRIMARY KEY,
            name VARCHAR(50), ranking INTEGER)"
rescue PG::Error => e
  puts e.message
ensure
  con&.close
end
