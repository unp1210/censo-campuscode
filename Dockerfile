FROM ruby:2.7.2

RUN apt-get update && apt-get install -y postgresql
RUN mkdir censo
WORKDIR /censo
ADD Gemfile ./Gemfile
ADD Gemfile.lock ./Gemfile.lock
RUN bundle install
ADD . /censo/