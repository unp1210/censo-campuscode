# frozen_string_literal: true

require 'byebug'
require 'faraday'
require 'json'
require 'sidekiq'
require 'table_print'
require 'pg'
require 'yaml'
Dir["#{File.dirname(__dir__)}/app/**/*.rb"].sort.each { |file| require file }
Dir["#{File.dirname(__dir__)}/db/*.rb"].sort.each { |file| require file }
